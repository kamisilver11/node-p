const express = require('express')
const router = express.Router()
let {people} = require('../data')

router.get('/',(req,res)=>{
    res.status(200).json({success:true,data:people})}
    )

  router.post('/', (req,res)=>{
    const {name} = req.body
    if(!name){
      return res.status(400).json({success:false,msg:'provide name'})
    }
    res.status(201).json({success:true,person:name})
  })


  router.put('/:id', (req, res) => {
    const { id } = req.params;
    const { name } = req.body;
    if (!name) {
      return res.status(400).json({ error: 'Name is required in the request body' });
    }
    res.json({ message: `Person with ID ${id} updated successfully with name: ${name}` });
  });

  router.delete('/:id', (req, res) => {
    const person = people.find((person) => person.id === Number(req.params.id))
    if (!person) {
      return res
        .status(404)
        .json({ success: false, msg: `no person with id ${req.params.id}` })
    }
    const newPeople = people.filter(
      (person) => person.id !== Number(req.params.id)
    )
    return res.status(200).json({ success: true, data: newPeople })
  })



module.exports = router
